package com.hxh.sp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

public class Sp {

	public static void main(String[] args) {
		// 串口打开后的串口对象
		SerialPort serialPort = null;
		// 串口的输入流对象 
		InputStream inputStream = null;

		Enumeration<?> en = CommPortIdentifier.getPortIdentifiers();
		CommPortIdentifier port = null;

		while (en.hasMoreElements()) {
			CommPortIdentifier portId = (CommPortIdentifier) en.nextElement();
			// 如果端口类型是串口, 则打印信息
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				System.out.println(portId.getName());
				// 选取我们需要的串口端口
				if (portId.getName().equals("COM2")) {
					port = portId;
				}
			}
		}

		if (port == null) {
			System.out.println("没有获取到需要的串口");
			return;
		}

		try {
			// 打开串口
			serialPort = (SerialPort) port.open("Main", 2000);
			// 获取串口的输入流对象
			inputStream = serialPort.getInputStream();
		} catch (PortInUseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		} catch (UnsupportedCommOperationException e) {
			e.printStackTrace();
		}

		byte[] readB = new byte[21];
		int nBytes = 0;
		while(true){
			try {
				while (inputStream.available() > 0) {
					nBytes = inputStream.read(readB);
				}
				// 将读出的字符数组数据，直接转换成十六进制。
				//StringToHex.printHexString(readB);
				System.out.println(new String(readB));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
